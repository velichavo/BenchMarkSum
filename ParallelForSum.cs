﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

namespace parallelSum
{
    public static class ParallelForSum
    {
        public static int LinqMethod(int[] arr)
        {
            int sum = 0;
            arr.AsParallel().ForAll(i => Interlocked.Add(ref sum, arr[i]));
            return sum;
        }
    }
}
