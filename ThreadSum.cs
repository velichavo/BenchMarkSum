﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace parallelSum
{
    public class ThreadSum
    {
        public static int TotalSum = 0;
        public static int[] Sum;
        public static int[] Arr;
        public static int CountOfThread;
        public static int[] BeginPos;
        public static int[] EndPos;
        public static int CountWaitThread;
        public static int ThreadSumMethod(int[] arr, int countThread)
        {
            Sum = new int[countThread];
            BeginPos = new int[countThread];
            EndPos = new int[countThread];
            Arr = arr;
            CountOfThread = arr.Length / countThread;
            CountWaitThread = 0;
            int remainder = arr.Length % countThread;
            for (int i = 0; i < countThread; i++)
            {
                BeginPos[i] = i * CountOfThread;
                EndPos[i] = i * CountOfThread + CountOfThread;
                if (i == countThread - 1) EndPos[i] += remainder;

                var t = new Thread(CommonSumMethod);
                t.IsBackground = true;
                t.Start(i);
                t.Join();

            }
            return TotalSum; ;
        }

        public static void CommonSumMethod(object cur)
        {
            int index = (int)(cur);
            Sum[index] = 0;
            int cp = BeginPos[index];
            int pp = EndPos[index];
            for (int i = cp; i < pp; i++)
            {
                Sum[index] += Arr[i];
            }
            Interlocked.Add(ref TotalSum, Sum[index]);
            //CountWaitThread += 1;
            //Console.WriteLine(CountWaitThread);
        }
    }
}
