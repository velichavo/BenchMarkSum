﻿using System;
using System.Diagnostics;
using System.Timers;
namespace parallelSum
{
    class Program
    {
        static void Main(string[] args)
        {
            //int count = 100_000;
            //int count = 1_000_000;
            int count = 10_000_000;
            Random random = new Random(2);
            int sum = 0;
            int[] arr = new int[count];
            for(int i = 0; i < count; i++)
            {
                arr[i] = random.Next(255);
            }
            // common
            Stopwatch sw = new Stopwatch();
            sw.Start();

            sum = CommonSum.CommonSumMethod(arr);

            sw.Stop();

            Console.WriteLine("Sum " + count + " = " + sum);
            Console.WriteLine("Time = " + sw.ElapsedMilliseconds);

            sw.Reset();
            // linq
            sw.Start();

            sum = ParallelForSum.LinqMethod(arr);

            sw.Stop();

            Console.WriteLine("Sum " + count + " = " + sum);
            Console.WriteLine("Time = " + sw.ElapsedMilliseconds);

            sw.Reset();
            //thread
            sw.Start();

            sum = ThreadSum.ThreadSumMethod(arr, 6);

            sw.Stop();

            Console.WriteLine("Sum " + count + " = " + sum);
            Console.WriteLine("Time = " + sw.ElapsedMilliseconds);

            Console.ReadKey();
        }
    }
}
