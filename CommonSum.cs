﻿using System;
using System.Collections.Generic;
using System.Text;

namespace parallelSum
{
    public static class CommonSum
    {
        public static int CommonSumMethod(int[] arr)
        {
            int sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                sum += arr[i];
            }

            return sum;
        }
    }
}
